<?php

foreach (glob(__DIR__ . '/*Test.php') as $file) {
	require $file;
}

class AllTests {
	public static function suite() {
		$suite = new PHPUnit_Framework_TestSuite('Masterstream Unit Tests');

		foreach (glob(__DIR__ .'/*Test.php') as $file) {
			$suite->addTestSuite(basename($file, '.php'));
		}

		return $suite;
	}
}
