<?php

/**
 * PDO can not work directly with PHPUnit by getMock('PDO'), until its constructor is disabled.
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class MockPDO extends \PDO {

	public function __construct () {

	}

}
