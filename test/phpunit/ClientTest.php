<?php

class ClientTest extends PHPUnit_Framework_TestCase {

	/**
	 * This setup is to mask all PDO connection. In case more deeper methods in PgsqlAdapter,
	 * even PDO have to be mocked or stubbed. You may either use:
	 * $mockConn = \ActiveRecord\ConnectionManager::set_connection('unittest');
	 * $pdoConn  = $mockConn->connection;
	 * to mock predefined methods here. Or simply assgin a new mock object with your setups.
	 * I may have to move this to an abstract BaseTest Class.
	 *
	 * (non-PHPdoc)
	 * @see PHPUnit_Framework_TestCase::setUp()
	 */
	public function setUp() {

		$config = \ActiveRecord\Config::instance();
		$config->set_default_connection('unittest');
			
		$connection_string = $config->get_connection('unittest');
		$info = \ActiveRecord\Connection::parse_connection_url($connection_string);

		$mockConn = $this->getMockBuilder('\ActiveRecord\MockpgsqlAdapter')
		->setConstructorArgs(array($info))
		->setMethods(array('columns', 'query'))
		->getMock();

		$mockConn->connection = $this->getMock('\MockPDO');
		\ActiveRecord\ConnectionManager::set_connection('unittest', $mockConn);

		/**
		 * @abstract This becomes important since all models are derived from BaseModel
		 * and the static instance could go beyond one particular test.
		 */
		\Masterstream\BaseModel::clear_instance();
	}

	/**
	 * Make sure it will include reference of its client company
	 */
	public function testBelongsTo() {
		$this->assertEquals( array(	'client_company',
    			'class'			=> 'Masterstream\ClientCompany',
    			'foreign_key'	=> 'client_company_id',
    			'readonly'		=> true ), \Masterstream\Client::$belongs_to[0]);
	}
	
	/**
	 * Make sure listed field will be checked upon presence
	 */
	public function testValidatesPresence() {
		foreach (\Masterstream\Client::$validates_presence_of as $field) {
			$this->assertTrue(in_array($field[0], array('client_company_id', 'firstname', 'lastname', 'phone', 'email')));
		}
	}
	
	public function testCheckValidLocationId() {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->disableOriginalConstructor()
		->setMethods(array('attribute_is_dirty', 'read_attribute', '__wakeup'))
		->getMock();
		$mockClient->expects($this->any())
		->method('attribute_is_dirty')
		->with('location_id')
		->will($this->onConsecutiveCalls(false,   // case 1, location_id is not dirty
										 true,    // case 2, location_id is dirty, but value as null
										 true,	  // case 3, location_id is dirty, but value as '' (empty string)
										 true,	  // case 4, location_id is dirty, with values other than case 3, 4
										 true )); // case 5, location_di is dirty, with values other than case 3, 4

		$mockClientCompany = $this->getMockBuilder('\Masterstream\ClientCompany')
		->disableOriginalConstructor()
		->setMethods(array('has_location', '__wakeup'))
		->getMock();
		$mockClientCompany->expects($this->exactly(2))
		->method('has_location')
		->with(123)
		->will($this->onConsecutiveCalls(true,		// case 4
										 false ));  // case 5
		
		$mockClient->expects($this->any())
		->method('read_attribute')
		->will($this->onConsecutiveCalls(null, // case 2
										 '',   // case 3
										 123, $mockClientCompany, 123,	  // case 4
										 123, $mockClientCompany, 123 )); // case 5

		$mockClient->check_valid_location_id(); // case 1 assertion, expecting no error
		$this->assertNull($mockClient->errors);

		$mockClient->check_valid_location_id(); // case 2 assertion, expecting no error
		$this->assertNull($mockClient->errors);

		$mockClient->check_valid_location_id(); // case 3 assertion, expecting no error
		$this->assertNull($mockClient->errors);
		
		$mockClient->check_valid_location_id(); // case 4 assertion, expecting no error
		$this->assertNull($mockClient->errors);
		
		$mockClient->check_valid_location_id(); // case 5 assertion, expecting validation error of the field
		$this->assertTrue($mockClient->errors->is_invalid('location_id'));
	}

	public function testSetUpdateDate() {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('table', '__wakeup'))
		->disableOriginalConstructor()
		->getMock();
		
		$mockARTable = $this->getMockBuilder('\ActiveRecord\Table')
		->disableOriginalConstructor()
		->getMock();

		$mockClient::staticExpects($this->any())
		->method('table')
		->will($this->returnValue($mockARTable));
		
		$mockClient->set_update_date();
		$this->assertEquals(date('Y-m-d'), $mockClient->read_attribute('last_updated'));
	}

	public function testSetInactiveFailed() {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('table', 'check_last_active_account', '__wakeup'))
		->disableOriginalConstructor()
		->getMock();
		
		$mockARTable = $this->getMockBuilder('\ActiveRecord\Table')
		->disableOriginalConstructor()
		->getMock();
		
		$mockClient::staticExpects($this->any())
		->method('table')
		->will($this->returnValue($mockARTable));
		
		$mockClient->expects($this->any())
		->method('check_last_active_account')
		->will($this->returnValue(false));
		
		$this->assertFalse($mockClient->set_inactive(true));
		$this->assertFalse($mockClient->set_inactive(1));
		$this->assertFalse($mockClient->set_inactive('true'));
	}

	public function dataSetInactiveSuccess() {
		return array(
			array(true, true, date('Y-m-d')),
			array(1, true, date('Y-m-d')),
			array('true', true, date('Y-m-d')),
			array(false, false, null),
			array(0, false, null),
			array('', false, null),
		);
	}

	/**
	 * @dataProvider dataSetInactiveSuccess
	 */
	public function testSetInactiveSuccess($input, $expd_inactive, $expd_inactive_date) {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('check_last_active_account', 'table', '__wakeup'))
		->disableOriginalConstructor()
		->getMock();
		
		$mockARTable = $this->getMockBuilder('\ActiveRecord\Table')
		->disableOriginalConstructor()
		->getMock();
		
		$mockClient::staticExpects($this->any())
		->method('table')
		->will($this->returnValue($mockARTable));
		
		$mockClient->expects($this->any())
		->method('check_last_active_account')
		->will($this->returnValue(true));
		
		$mockClient->set_inactive($input);
		$this->assertEquals($expd_inactive, $mockClient->read_attribute('inactive'));
		$this->assertEquals($expd_inactive_date, $mockClient->read_attribute('inactive_date'));
	}

	public function testActivate() {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('save', 'table', '__wakeup'))
		->disableOriginalConstructor()
		->getMock();
		
		$mockARTable = $this->getMockBuilder('\ActiveRecord\Table')
		->disableOriginalConstructor()
		->getMock();
		
		$mockClient::staticExpects($this->any())
		->method('table')
		->will($this->returnValue($mockARTable));
		
		$mockClient->expects($this->any())
		->method('save')
		->will($this->returnValue(null));

		$mockClient->activate();
		$this->assertEquals(false, $mockClient->read_attribute('inactive'));
		$this->assertNull($mockClient->read_attribute('inactive_date'));
	}

	public function dataDeactivate() {
		return array (
			array(false, false),
			array(true, 'save() result')
		);
	}
	
	/**
	 * @dataProvider dataDeactivate
	 */
	public function testDeactivate($set_inactive_return_value, $expd) {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('save', 'set_inactive', 'table', '__wakeup'))
		->disableOriginalConstructor()
		->getMock();
		
		$mockARTable = $this->getMockBuilder('\ActiveRecord\Table')
		->disableOriginalConstructor()
		->getMock();
		
		$mockClient::staticExpects($this->any())
		->method('table')
		->will($this->returnValue($mockARTable));
		
		$mockClient->expects($this->any())
		->method('set_inactive')
		->with(true)
		->will($this->returnValue($set_inactive_return_value));

		$mockClient->expects($this->any())
		->method('save')
		->will($this->returnValue('save() result'));

		$this->assertEquals($expd, $mockClient->deactivate());
	}

	/**
	 * __wakeup needs to be mocked, the Model seems to restore a connection when being deserialzed.
	 * read_attribute() to be mocked, to offer stub on $this->id, or other properties
	 * table() is mocked, to cut off unnecessary db connection
	 *
	 * For this test, I want to protect the actual composed SQL, as well as it's result.
	 *
	 */
	public function testCheckLastActiveAccount() {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('read_attribute', 'table', '__wakeup'))
		->disableOriginalConstructor()
		->getMock();
		$mockClient->expects($this->any())
		->method('read_attribute')
		->will($this->onConsecutiveCalls(123, 321)); // to mock PDO bind values
		
		$mockARTable = $this->getMockBuilder('\ActiveRecord\Table')
		->disableOriginalConstructor()
		->setMethods(array('find_by_sql', 'get_fully_qualified_table_name'))
		->getMock();
		$mockARTable->conn = new stdClass();
		$mockARTable->expects($this->once())
		->method('find_by_sql')
		->with("SELECT * FROM tablename WHERE id= ? AND (inactive = 't' OR (SELECT COUNT(*) FROM clients WHERE client_company_id = ? AND inactive = 'f') > 1)"
				, array(123, 321))
		->will($this->returnValue(null));
		$mockARTable->expects($this->once())
		->method('get_fully_qualified_table_name')
		->will($this->returnValue('tablename'));
		
		$mockClient::staticExpects($this->any())
		->method('table')
		->will($this->returnValue($mockARTable));
		
		$this->assertFalse($mockClient->check_last_active_account());
		$this->assertTrue($mockClient->errors->is_invalid('id'));
	}

	/**
	 * This is similar to above one, but I only want to test the result in this case. Thus no deeper mocks.
	 */
	public function testCheckLastActiveAccountSuccess() {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->disableOriginalConstructor()
		->setMethods(array('find', 'read_attribute', '__wakeup'))
		->getMock();
		$mockClient::staticExpects($this->once())
		->method('find')
		->will($this->returnValue(true));
		
		$this->assertTrue($mockClient->check_last_active_account());
	}
	
	/**
	 * case 1, when $this->id is absent, meaning the object is not persisted yet;
	 * case 2, RFQ count() does not find rows;
	 * case 3, RFQ count() find rows, which in turn raise error in $this->errors;
	 *
	 */
	public function testCheckHavingRfqs() {
		
		$mockOppClient = $this->getMockBuilder('\Masterstream\RFQ')
		->disableOriginalConstructor()
		->setMethods(array('count'))
		->getMock();
		
		// I want to make sure the count is called with corrent parameters
		$mockOppClient->staticExpects($this->any())
		->method('count')
		->with(array('conditions' => array('client_id = ?', 12)))
		->will($this->onConsecutiveCalls(0,		// case 2
										 1));	// case 3

		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('read_attribute'))
		->disableOriginalConstructor()
		->getMock();
		$mockClient->expects($this->any())
		->method('read_attribute') // to mock $this->id, it's not ideal...
		->will($this->onConsecutiveCalls( null,					// case 1
										  1, 12,				// case 2
										  1, 12, 123, 123));	// case 3

		$mockClient::set_instance($mockOppClient);
		$this->assertFalse($mockClient->check_having_rfqs());		// case 1
		$this->assertTrue($mockClient->check_having_rfqs());		// case 2
		$this->assertFalse($mockClient->check_having_rfqs());		// case 3
		$this->assertTrue($mockClient->errors->is_invalid('id'));
	}

	/**
	 * case 1, when $this->id is absent, meaning the object is not persisted yet;
	 * case 2, RFQ count() does not find rows;
	 * case 3, RFQ count() find rows, which in turn raise error in $this->errors;
	 *
	 */
	public function testCheckOpportunityClient() {
		
		$mockOppClient = $this->getMockBuilder('\Masterstream\Opportunity\Client')
		->disableOriginalConstructor()
		->setMethods(array('count'))
		->getMock();
		
		// I want to make sure the count is called with corrent parameters
		$mockOppClient->staticExpects($this->any())
		->method('count')
		->with(array('conditions' => array('client_id = ?', 12)))
		->will($this->onConsecutiveCalls(0,		// case 2
										 1));	// case 3

		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('read_attribute'))
		->disableOriginalConstructor()
		->getMock();
		$mockClient->expects($this->any())
		->method('read_attribute') // to mock $this->id, it's not ideal...
		->will($this->onConsecutiveCalls( null,					// case 1
										  1, 12,				// case 2
										  1, 12, 123, 123));	// case 3

		$mockClient::set_instance($mockOppClient);
		$this->assertFalse($mockClient->check_opportunity_client());	// case 1
		$this->assertTrue($mockClient->check_opportunity_client());		// case 2
		$this->assertFalse($mockClient->check_opportunity_client());	// case 3
		$this->assertTrue($mockClient->errors->is_invalid('id'));
	}

	/**
	 * This test is a little tricky. What I test here is
	 * to make sure before_destroy is called when is_deleteable() is called
	 *
	 * This test could be better to mock before_destroy, and let invoke() actually run
	 *
	 */
	public function testIsDeleteable() {
		$mockClient = $this->getMockBuilder('\Masterstream\Client')
		->setMethods(array('table', '__wakeup'))
		->disableOriginalConstructor()
		->getMock();

		$mockARTable = $this->getMockBuilder('\ActiveRecord\Table')
		->disableOriginalConstructor()
		->getMock();
		$callback = $this->getMockBuilder('\ActiveRecord\Callback')
		->disableOriginalConstructor()
		->setMethods(array('invoke'))
		->getMock();
		$callback->expects($this->once())
		->method('invoke')
		->with($mockClient, 'before_destroy', false)
		->will($this->returnValue(null));
		
		$mockARTable->callback = $callback;
		$mockClient::staticExpects($this->any())
		->method('table')
		->will($this->returnValue($mockARTable));

		$this->assertFalse($mockClient->is_deleteable());
	}
	
}