<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * This is a shortcut to let AR's ConnectionManager know this is a test enviornment,
 * therefore allow injection of mock objects. In reality, following line should
 * be removed. The functionality should solely rely on enviornment settings.
 *
 */
if ('DEVVM' != getenv('MS_STACK')) putenv('MS_STACK=DEVVM');

/**
 * Auto load all target (Model) classes into test space.
 * $_SERVER['SERVER_NAME'] is just to satisfy AR's configuration settings, actual
 * test is performed on mockPDO entirely. Without this, you may see lots of PHP
 * notice in error_log.
 */
$_SERVER['SERVER_NAME'] = 'devvm';
require_once __DIR__ . '/../../models/autoload.php';

/**
 * All adapter classes should be loaded by php-activerecord automatically. But
 * in PHPUnit enviornment, things are bit odd when I have to create a mock
 * MockpgsqlAdapter before phpAR actually load it. It will throw an exception
 * of redeclaring the adapter class. So the following 'require' has to be in
 * place before getting into any unit tests, if a connection is depended.
 *
 */
require_once __DIR__ . '/../../inc/php-activerecord/lib/adapters/MockpgsqlAdapter.php';
require_once __DIR__ . '/helper/MockPDO.php';
