<?php
/**
 * @package ActiveRecord
 */
namespace ActiveRecord;

require_once 'PgsqlAdapter.php';

/**
 * Mock adapter used for unit testing. the connection is set to null where it
 * is supposedly a PDO instance. By setting php-activerecord default connection
 * to 'unittest', this adapter will be injected. For testing PDO statement
 * codes. you may create a mock object of this class and explicitly set on the
 * Connection object:
 *
 * $mockAdapter = $this->getMock('MockpgsqlAdapter');
 * ConnectionManager::set_connection('unittest', $mockAdapter);
 * Note, $mockAdapter is mock of MockpgsqlAdapter, not PgsqlAdapter
 *
 * @author syang <simonxy@gmail.com>
 *
 */
class MockpgsqlAdapter extends PgsqlAdapter {
	
	public function __construct($info) {
		// unix sockets start with a /
		if ($info->host[0] != '/')
		{
		$host = "host=$info->host";
	
		if (isset($info->port))
			$host .= ";port=$info->port";
		}
		else
			$host = "unix_socket=$info->host";
	
			$this->connection = null;
			$this->dbname = $info->db;
	}

}
