<?php

namespace Msst\ClientCompany;

class Note extends \Msst\BaseModel {

    static $table_name = 'client_company_notes';

    static $primary_key = 'id';
}