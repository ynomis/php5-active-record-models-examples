<?php

namespace Msst\ClientCompany;

class Location extends \Msst\BaseModel {

    static $table_name = 'client_company_locations';

    static $primary_key = 'id';
}