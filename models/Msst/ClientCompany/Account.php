<?php

namespace Msst\ClientCompany;

class Account extends \Msst\BaseModel {

    static $table_name = 'client_company_accounts';

    static $primary_key = 'id';
}