<?php

namespace Msst\ClientCompany;

class Requirement extends \Msst\BaseModel {

    static $table_name = 'client_company_requirements';

    static $primary_key = 'id';
}