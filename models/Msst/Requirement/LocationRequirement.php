<?php

namespace Msst\Requirement;

class LocationRequirement extends \Msst\BaseModel {

    static $table_name = 'req_sq_idx';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('requirement', 'class' => 'Msst\Requirement', 'foreign_key' => 'req_sq_id')
    );

    /**
     * A reference to its rfq object.
     * May need this to check ma_company_id.
     * So user can use:
     * $loc_req = \Msst\Requirement\LocationRequirement::find(1);
     * $rfq = $loc_req->rfq; /* var \Msst\RFQ $rfq *\/
     *
     * @return \Msst\RFQ | null, if not found
     */
    public function get_rfq() {
        return \Msst\RFQ::first(array(
            'joins' => 'INNER JOIN rfq_idx ON rfq_idx.rfq_id = rfq.id
						INNER JOIN req_sq ON req_sq.rfq_idx_id = rfq_idx.id
						INNER JOIN req_sq_idx ON req_sq_idx.req_sq_id = req_sq.id',
            'conditions' => array('req_sq_idx.id = ?', $this->id)
        ));
    }

}