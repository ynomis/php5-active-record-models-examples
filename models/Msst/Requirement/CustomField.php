<?php

namespace Msst\Requirement;

class CustomField extends \Msst\BaseModel {

    static $table_name = 'req_sq_form_data';

    static $primary_key = array('ma_company_id', 'table_name', 'input_id');

    static $belongs_to = array(
        array('macompany', 'class' => 'Msst\MACompany', 'foreign_key' => 'ma_company_id')
    );

    /**
     * This is to grab label for dynamic fields from req_sq_idx.
     * On Sep. 20th, it's requested to handle 2 cases, with or wihout '::'.
     * It's suboptimal though. Think of this as a hack for now. -syang
     *
     * @param string $value
     */
    public static function get_dynamic_field_label($value) {
        if (false == strpos($value, '::')) {
            $label = $value; // Case 1: $menu_display_name
        } else {
            $_temp = explode('::', $value); // Case 2: "{$sku}::{$menu_display_name}::{$amount}"
            $label = $_temp[1];
        }
        
        return $label;
    }

}