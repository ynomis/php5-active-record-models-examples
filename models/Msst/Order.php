<?php

namespace Msst;

/**
 * Order.
 * So far it has one associate object, payment of class Msst\Order\Payment
 * Table name: orders
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class Order extends \Msst\BaseModel {

    static $table_name = 'orders';

    static $primary_key = 'id';

    /**
     * Relationship - has one Msst\Order\Payment object.
     * $order->payment;
     */
    static $has_one = array(
        array('payment', 'class' => 'Msst\Order\Payment', 'foreign_key' => 'order_id')
    );

    /**
     * A reference to its rfq object.
     * So user can use:
     * $order = \Msst\Order::find(1);
     * $rfq = $order->rfq; /* var \Msst\RFQ $rfq *\/
     *
     * @return \Msst\RFQ | null, if not found
     */
    public function get_rfq() {
        return \Msst\RFQ::first(array(
            'joins' => 'INNER JOIN rfq_idx ON rfq_idx.rfq_id = rfq.id
						INNER JOIN orders ON orders.rfq_idx_id = rfq_idx.id', 'conditions' => array('orders.id = ?', $this->id)));
    }
}