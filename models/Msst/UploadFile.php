<?php

namespace Msst;

/**
 * Upload files.
 * Table name: uploadfiles
 *
 * @author Simon Yang <simonxy@gmail.com>
 *        
 */
class UploadFile extends \Msst\BaseModel {

    static $table_name = 'uploadfiles';

    static $primary_key = 'id';
}