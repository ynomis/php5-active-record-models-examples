<?php

namespace Msst;

/**
 * Parent Requirement from a RFQ.
 * Table name: req_sq
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class Requirement extends \Msst\BaseModel {

    static $table_name = 'req_sq';

    static $primary_key = 'id';

    static $has_many = array(
        array('loc_requirements', 'class' => 'Msst\Requirement\LocationRequirement', 'foreign_key' => 'req_sq_id')
    );

    /**
     * A reference to its rfq object.
     * May be used to check its ma_company_id.
     * So user can use:
     * $req = \Msst\Requirement::find(1);
     * $rfq = $req->rfq; /* var \Msst\RFQ $rfq *\/
     *
     * @return \Msst\RFQ | null, if not found
     */
    public function get_rfq() {
        return \Msst\RFQ::first(array(
            'joins' => 'INNER JOIN rfq_idx ON rfq_idx.rfq_id = rfq.id
						INNER JOIN req_sq ON req_sq.rfq_idx_id = rfq_idx.id', 'conditions' => array('req_sq.id = ?', $this->id)));
    }
}