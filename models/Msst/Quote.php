<?php

namespace Msst;

/**
 * Quote.
 * A quote based on RFQ requirement. Since location is always involved, its
 * children objects LocationQuote carries more detail info.
 * Table name: quote_sq
 *
 * @see Msst\Quote\LocationQuote
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class Quote extends \Msst\BaseModel {

    static $table_name = 'quote_sq';

    static $primary_key = 'id';

    /**
     * Relationship - has many Msst\Quote\LocationQuote.
     * $quote->location_quotes /* @var array *\/
     */
    static $has_many = array(
        array('location_quotes', 'class' => 'Msst\Quote\LocationQuote', 'foreign_key' => 'quote_sq_id')
    );

    /**
     * A reference to its rfq object.
     * May be used to check its ma_company_id.
     * So user can use:
     * $quote = \Msst\Quote::find(1);
     * $rfq = $quote->rfq; /* var \Msst\RFQ $rfq *\/
     *
     * @return \Msst\RFQ | null, if not found
     */
    public function get_rfq() {
        return \Msst\RFQ::first(array(
            'joins' => 'INNER JOIN rfq_idx ON rfq_idx.rfq_id = rfq.id
						INNER JOIN quote_sq ON quote_sq.rfq_idx_id = rfq_idx.id', 'conditions' => array('quote_sq.id = ?', $this->id)));
    }
}