<?php

namespace Msst;

/**
 * CRM Opportunity.
 * Used by CRM module.
 * Table name: crm_opportunities
 *
 * @author Simon Yang <simonxy@gmail.com>
 *        
 */
class Opportunity extends \Msst\BaseModel {

    static $table_name = 'crm_opportunities';

    static $primary_key = 'id';
}