<?php

namespace Msst;

/**
 * The largest business entity in masterstream model space.
 * It owns other entities
 * such as master agent, agent, client company, client.
 * Table name: ma_companies
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class MACompany extends \Msst\BaseModel {

    static $table_name = 'ma_companies';

    static $primary_key = 'id';

    /**
     * Relationship - has one Msst\MACompany\SystemSetup object.
     * Convenient access to its main setup object.
     */
    static $has_one = array(
        array('system_setup', 'class' => 'Msst\MACompany\SystemSetup', 'primary_key' => 'id', 'foreign_key' => 'ma_company_id')
    );
    CONST FEATURE_DOCUSIGN      = 1;
    CONST FEATURE_ANPI_BILLING  = 2; // See Ticket#719 for detail,
    
    /**
     * Helper function to check if a specific feature is enabled
     *
     * @see Msst\MACompany\SystemSetup isFeatureEnabled($feature)
     * @param int $feature
     * @return boolean
     */
    public function isFeatureEnabled($feature) {
        return $this->system_setup->isFeatureEnabled($feature);
    }

    /**
     * Helper function to set a specific feature on a MA company
     *
     * @see Msst\MACompany\SystemSetup enableFeature($feature)
     * @param int $feature
     * @return boolean
     */
    public function enableFeature($feature) {
        return $this->system_setup->enableFeature($feature);
    }

    /**
     * Helper function to clear a specific feature on a MA company
     *
     * @see Msst\MACompany\SystemSetup disableFeature($feature)
     * @param int $feature
     * @return boolean
     */
    public function disableFeature($feature) {
        return $this->system_setup->disableFeature($feature);
    }

}