<?php

namespace Msst\Helper;

class State {

    /**
     * return US states in collection of abbriviattion/full name pair
     *
     * @return array
     */
    public static function all_states() {
        $conn = \Msst\BaseModel::connection();
        
        $sql = "SELECT * FROM states ORDER BY state_full";
        $sth = $conn->query($sql);
        $result = $sth->fetchAll(\PDO::FETCH_FUNC, function ($state, $state_full) {
            return array('abbr' => $state, 'fullname' => ucwords(strtolower($state_full)));
        });
        
        return $result;
    }
}