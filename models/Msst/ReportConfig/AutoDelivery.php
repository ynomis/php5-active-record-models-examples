<?php

namespace Msst\ReportConfig;

class AutoDelivery extends \Msst\BaseModel {

    static $table_name = 'report_config_auto_delivery';

    static $primary_key = 'id';

    static $has_many = array(
        array('logs', 'class_name' => 'Msst\ReportConfig\AutoDelivery\Log', 'primary_key' => 'id', 'order' => 'ts asc'));

    static $validates_presence_of = array(
        array('report_id'),
        array('user_id'),
        array('user_type')
    );

    static $before_save = array('set_update_date');

    static $before_create = array('set_create_date');

    static $before_destroy = array('cleanup_references');

    /**
     * Set create date
     */
    public function set_create_date() {
        $this->assign_attribute('created_on', date("Y-m-d H:i:s"));
    }

    /**
     * Set update date
     */
    public function set_update_date() {
        $this->assign_attribute('modified_on', date("Y-m-d H:i:s"));
    }

    public function cleanup_references() {
        \Msst\ReportConfig\AutoDelivery\Log::delete_all(array(
            'conditions' => array('report_config_auto_delivery_id = ?', $this->id)
        ));
    }

}
