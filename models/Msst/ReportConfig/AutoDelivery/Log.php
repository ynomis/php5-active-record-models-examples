<?php

namespace Msst\ReportConfig\AutoDelivery;

class Log extends \Msst\BaseModel {

    static $table_name = 'report_config_auto_delivery_log';

    static $primary_key = 'id';

    static $validates_presence_of = array(
        array('report_config_auto_delivery_id'),
        array('log_message_type'),
        array('log_message')
    );

    static $before_create = array('set_ts');

    /**
     * Set timestamp
     */
    public function set_ts() {
        $this->assign_attribute('ts', date("Y-m-d H:i:s"));
    }

}
