<?php

namespace Msst;

use ActiveRecord\Errors;

/**
 * Client.
 * Tablename: clients
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class Client extends \Msst\BaseModel {

    static $table_name = 'clients';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('client_company', 'class' => 'Msst\ClientCompany', 'foreign_key' => 'client_company_id', 'readonly' => true)
    );

    static $validates_presence_of = array(
        array('client_company_id', 'message' => 'The client must belong to a client company.'),
        array('firstname'),
        array('lastname'),
        array('phone'),
        array('email')
    );

    /**
     * For now, this holds the list of attributes to be skipped validation of
     * their presence when its client company is a prospect or candidate
     *
     * @var array
     */
    static $validates_presence_on_condition = array(
//     	array('email'),
	);

    const REGEX_USPHONE = '/^[2-9]\d{2}-\d{3}-\d{4}$/';

    static $validates_format_of = array(
        // array('phone',     'with' => self::REGEX_USPHONE ),
        // array('alt_phone', 'with' => self::REGEX_USPHONE, 'allow_null' => true ),
        // array('mobile',    'with' => self::REGEX_USPHONE, 'allow_null' => true ),
        // array('fax',       'with' => self::REGEX_USPHONE, 'allow_null' => true ),
	);

    public static $after_validation = array(
        'check_presence_on_condition',
        'check_valid_location_id'
    );

    public static $before_save = array(
        'set_update_date'
    );

    public static $before_destroy = array(
        'check_last_active_account',
        'check_having_rfqs',
        'check_opportunity_client'
    );

    /**
     * Conditional validation rules.
     * 1, Do not validate presence of attrs when its client_company instance
     * come from a lead source, $this->client_company->lead === true.
     * Otherwise do.
     */
    public function check_presence_on_condition() {
        if (!$this->client_company->lead) {
            $configuration = array(
                'on'            => 'save',
                'allow_null'    => false,
                'allow_blank'   => false,
                'message'       => \ActiveRecord\Errors::$DEFAULT_ERROR_MESSAGES['blank']
            );
            
            foreach ( self::$validates_presence_on_condition as $attr ) {
                $options = array_merge($configuration, $attr);
                $this->errors->add_on_blank($options[0], $options['message']);
            }
        }
    }

    /**
     * Since we don't have a foreign key constraint on this, it has to be validated
     * here.
     * If location_id is provided, it has to be a valid one on the client
     * company. NULL or empty value is allowed.
     */
    public function check_valid_location_id() {
        if ($this->attribute_is_dirty('location_id') && $this->location_id) {
            $cltco = $this->client_company; /* @var $cltco \Msst\ClientCompany */
            
            if (!$cltco->has_location($this->location_id)) {
                if (!$this->errors) {
                    $this->errors = new \ActiveRecord\Errors($this);
                }
                $this->errors->add('location_id', 'Location \'' . $this->location_id . '\' could not be found.');
            }
        }
    }

    public function set_update_date() {
        $this->assign_attribute('last_updated', date('Y-m-d'));
    }

    /**
     * Setter for inactive attribute, need to check last active stuff before setting
     * it to true (deactivate).
     * You may want to check $this->errors for errors
     *
     * @param bool $value
     * @return boolean false, if setting is not successful
     */
    public function set_inactive($value) {
        if ((bool) $value && !$this->check_last_active_account()) {
            return false;
        }
        
        $this->assign_attribute('inactive', (bool) $value);
        $this->assign_attribute('inactive_date', (bool) $value ? date('Y-m-d') : null);
    }

    /**
     * Activate a client, set its inactive status to false.
     *
     * @param $ignore_validation, true
     *            - to bypass validation; false, otherwise. default to false.
     * @return boolean
     */
    public function activate($ignore_validation = false) {
        $this->inactive = false;
        $this->inactive_date = null;
        return $this->save(!$ignore_validation);
    }

    /**
     * Deactivate a client
     *
     * @return boolean, true - successfully deactivated, otherwise false.
     */
    public function deactivate() {
        if ($this->set_inactive(true) !== false) {
            return $this->save();
        }
        return false;
    }

    /**
     * check if this is the last active account for the company
     *
     * @internal I put a note here, for boolean value, you should use string literal. Because
     *           it's running PDO->execute($params) on this, and $params only accepts string.
     *
     * @return bool, true if it's inactive or not last active account, so it's ok to deactivate/destroy.
     *         false, if otherwise
     */
    public function check_last_active_account() {
        $delete = static::find(array(
            'conditions' => array('id= ? AND (inactive = \'t\' OR (SELECT COUNT(*) FROM clients ' .
                'WHERE client_company_id = ? AND inactive = \'f\') > 1)', $this->id, $this->client_company_id)));
        
        if (is_null($delete)) {
            if (!$this->errors) {
                $this->errors = new \ActiveRecord\Errors($this);
            }
            $this->errors->add('id', "You may not delete/deactivate the last active account with in the client company, [{$this->id}]");
            
            return false;
        }
        
        return true;
    }

    /**
     * This is used mainly by validation on deletion.
     *
     * @return boolean, true - if this client doesn't have any records in rfqs, so it's ok to delete
     *         false - this client has rfqs, so can not be deleted
     */
    public function check_having_rfqs() {
        if (!$this->id)
            return false;
        
        $return = !(bool) RFQ::count(array('conditions' => array('client_id = ?', $this->id)));
        
        if (!$return) {
            if (!$this->errors) {
                $this->errors = new \ActiveRecord\Errors($this);
            }
            $this->errors->add('id', "You may not delete client [{$this->id}] when there are rfqs bound to it.");
        }
        
        return $return;
    }

    /**
     * This is used mainly by validation of deletion
     *
     * @return boolean, true - if this client doesn't have entry in Opportunity, so it's ok to delete
     *         false - this client bound to a opportunity, so can not be deleted
     */
    public function check_opportunity_client() {
        if (!$this->id)
            return false;
        
        $return = !(bool) Opportunity\Client::count(array(
            'conditions' => array('client_id = ?', $this->id)
        ));
        
        if (!$return) {
            if (!$this->errors) {
                $this->errors = new \ActiveRecord\Errors($this);
            }
            $this->errors->add('id', "You may not delete client [{$this->id}] when it is bound to opportunity client.");
        }
        
        return $return;
    }

    /**
     * Convenient function to aggregate all check functions registed in 'before_destroy'
     *
     * @return boolean, true - ok to delete; false otherwise
     */
    public function is_deleteable() {
        return (bool) static::table()->callback->invoke($this, 'before_destroy', false);
    }
}