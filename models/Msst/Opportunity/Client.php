<?php

namespace Msst\Opportunity;

class Client extends \Msst\BaseModel {

    static $table_name = 'crm_opportunity_clients';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('opportunity', 'class' => 'Msst\Opportunity', 'foreign_key' => 'opportunity_id')
    );

    static $has_one = array(
        array('parent_client', 'class' => 'Msst\Client', 'foreign_key' => 'client_id')
    );

}