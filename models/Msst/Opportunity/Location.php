<?php

namespace Msst\Opportunity;

class Location extends \Msst\BaseModel {

    static $table_name = 'crm_opportunity_locations';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('opportunity', 'class' => 'Msst\Opportunity', 'foreign_key' => 'opportunity_id')
    );

    static $has_one = array(
        array('parent_location', 'class' => 'Msst\ClientCompany\Location', 'foreign_key' => 'client_company_location_id')
    );

}