<?php

namespace Msst\Opportunity;

class Notes extends \Msst\BaseModel {

    static $table_name = 'crm_opportunity_notes';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('opportunity', 'class' => 'Msst\Opportunity', 'foreign_key' => 'opportunity_id')
    );

}