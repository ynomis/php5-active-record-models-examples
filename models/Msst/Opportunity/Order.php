<?php

namespace Msst\Opportunity;

class Order extends \Msst\BaseModel {

    static $table_name = 'crm_opportunity_orders';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('opportunity', 'class' => 'Msst\Opportunity', 'foreign_key' => 'opportunity_id')
    );

    static $has_one = array(
        array('parent_order', 'class' => 'Msst\Order', 'foreign_key' => 'order_id')
    );

}