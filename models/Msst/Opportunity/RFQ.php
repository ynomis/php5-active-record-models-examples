<?php

namespace Msst\Opportunity;

class RFQ extends \Msst\BaseModel {

    static $table_name = 'crm_opportunity_rfqs';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('opportunity', 'class' => 'Msst\Opportunity', 'foreign_key' => 'opportunity_id')
    );

    static $has_one = array(
        array('rfq', 'class' => 'Msst\RFQ', 'foreign_key' => 'rfq_id')
    );

}