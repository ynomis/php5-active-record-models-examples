<?php

namespace Msst;

/**
 * RFQ.
 * Table name: rfq
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class RFQ extends \Msst\BaseModel {

    static $table_name = 'rfq';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('client', 'class' => 'Msst\Client', 'foreign_key' => 'client_id')
    );

    static $has_one = array(
        array('rfq_idx', 'class' => 'Msst\RFQ\RFQIdx', 'foreign_key' => 'rfq_id')
    );

}