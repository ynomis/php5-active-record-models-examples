<?php

namespace Msst;

/**
 * Quote Provider.
 * Table name: quote_providers
 *
 * @author Simon Yang <simonxy@gmail.com>
 *        
 */
class QuoteProvider extends \Msst\BaseModel {

    static $table_name = 'quote_providers';

    static $primary_key = 'id';
}