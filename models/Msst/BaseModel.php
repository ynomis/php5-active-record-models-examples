<?php

namespace Msst;

/**
 *
 *
 * Base model that all Msst models should extend, so we can setup general
 * rules on our own models without altering vendor's.
 *
 * @author simon
 *
 */
class BaseModel extends \ActiveRecord\Model {

    /**
     * This is actually static::$validates_length_of.
     * Since I have no way to
     * add to static class variable through all inherited classes, I have
     * to create those fields dynamically and cache them here. And I also have
     * to process it in after_validation hookup.
     *
     * @var array
     *
     */
    private static $varchar_attr_cache = array();

    /**
     * I add maximum length check on all varchar fields with their limit set
     * on database. 'cause Postgres would complain and stop executing SQL in
     * that case.
     */
    public function after_construct() {
        $classname = get_called_class();
        
        if (is_subclass_of($this, __CLASS__) && !array_key_exists($classname, self::$varchar_attr_cache)) {
            $columns = static::table()->columns;
            
            foreach ( $columns as $key => $col ) {
                if ('varchar' == $col->raw_type && $col->length > 0) {
                    self::$varchar_attr_cache[$classname][] = array($key, 'maximum' => $col->length);
                }
            }
        }
    
    /**
     * Here is an example I can register a method that runs before others
     * I don't need this anymore, but I want to keep it as comment here to
     * show how to register a method in before|after action in base model.
     *
     * static::table()->callback->register(
     * 'before_update', 'ignore_protected_fields', array('prepend' => true));
     */
    
    /**
     * Since I modified register method in Callback to observe both statically
     * defined callbacks as well as implicit functions, I can now just define
     * public function before_update() {
     * }
     * here is base model to cases such as protect some read-only attributes.
     * For detail implementation, I don't think it through yet, will add that
     * later.
     */
    }

    /**
     *
     *
     * I put the default Postgres varchar length check before calling
     * ActiveRecord's save(). I don't want to use any before|after hook,
     * 'cause it may override those functions in subclasses in
     * conventioal ways.
     *
     * @see \ActiveRecord\Model::save()
     */
    public function save($validate = true) {
        $classname = get_called_class();
        $attrs = self::$varchar_attr_cache[$classname];
        
        if ($attrs) {
            $validator = new \ActiveRecord\Validations($this);
            
            $validator->validates_length_of($attrs);
            $this->errors = $validator->get_record();
            
            if (!$this->errors->is_empty()) {
                return false;
            }
        }
        
        return parent::save($validate);
    }

    /**
     * This and following methods are for unit test purpose ONLY.
     * Production
     * enviornment should completely ignore this.
     * In test enviornment, all models inheritted from this class could have
     * this instance set as a mock object which would offer easy mock stub
     * for static methods. Note, all derived models are sharing the exact same
     * mock instance.
     *
     * @var BaseModel
     */
    private static $instance;

    /**
     * Most likely the parameter $obj will be the PHPUnit mock class of derived
     * class of self.
     *
     * @param BaseModel $obj
     */
    public static function set_instance(BaseModel $obj) {
        if ('DEVVM' == getenv('MS_STACK'))
            self::$instance = $obj;
        else
            return false;
    }

    /**
     * This is to clear the instance set for unit test.
     * It's effective in unit
     * test only.
     */
    public static function clear_instance() {
        self::$instance = null;
    }

    /**
     * Intercepts static find call for easy mocking in unit testing enviornment.
     *
     * @return mixed
     */
    public static function find() {
        if (self::$instance) {
            return forward_static_call_array(array(self::$instance, 'find'), func_get_args());
        } else {
            return forward_static_call_array('parent::find', func_get_args());
        }
    }

    /**
     * Intercepts static find call for easy mocking in unit testing enviornment.
     *
     * @return mixed
     */
    public static function count() {
        if (self::$instance) {
            return forward_static_call_array(array(self::$instance, 'count'), func_get_args());
        } else {
            return forward_static_call_array('parent::count', func_get_args());
        }
    }

}