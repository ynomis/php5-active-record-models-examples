<?php

namespace Msst;

/**
 * Agent Company.
 * An agent company belongs to a master agent, has many agents.
 * Table name: agent_companies
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class AgentCompany extends \Msst\BaseModel {

    static $table_name = 'agent_companies';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('master_agent', 'class' => '\Msst\MasterAgent', 'foreign_key' => 'ma_id')
    );

    static $has_many = array(
        array('agents', 'class' => '\Msst\Agent', 'foreign_key' => 'agent_company_id')
    );
}