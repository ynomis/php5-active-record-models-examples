<?php

namespace Msst;

/**
 * Master Agent.
 * Table name: master_agents
 *
 * @author Simon Yang <simonxy@gmail.com>
 *        
 */
class MasterAgent extends \Msst\BaseModel {

    static $table_name = 'master_agents';

    static $primary_key = 'id';
}