<?php

namespace Msst;

/**
 * Agent, belongs to Agent Company, which in turn belongs to Master Agent.
 * Tale name: agents
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class Agent extends \Msst\BaseModel {

    static $table_name = 'agents';

    static $primary_key = 'id';

    /**
     * Defines association relationship with \Msst\MasterAgent and \Msst\AgentCompany
     *
     * @var Array
     */
    static $belongs_to = array(
        array('master_agent', 'class' => '\Msst\MasterAgent', 'foreign_key' => 'ma_id'),
        array('agent_company', 'class' => '\Msst\AgentCompany', 'foreign_key' => 'agent_company_id')
    );
}