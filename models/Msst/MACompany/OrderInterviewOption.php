<?php

namespace Msst\MACompany;

class OrderInterviewOption extends \Msst\BaseModel {

    static $table_name = 'ma_system_setup_order_interview_question_options';

    static $primary_key = array('ma_company_id', 'custom_question_id', 'sequence_id');

    static $belongs_to = array(
        array('macompany', 'class' => 'Msst\MACompany', 'foreign_key' => 'ma_company_id'),
        array('question', 'class' => 'Msst\MACompany\OrderInterviewQuestion', 'foreign_key' => 'custom_question_id')
    );

}