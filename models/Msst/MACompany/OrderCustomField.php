<?php

namespace Msst\MACompany;

class OrderCustomField extends \Msst\BaseModel {

    static $table_name = 'ma_system_setup_order_custom_fields';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('macompany', 'class' => 'Msst\MACompany', 'foreign_key' => 'ma_company_id')
    );

    static $has_many = array(
        array('options', 'class' => 'Msst\MACompany\OrderCustomFieldOption', 'foreign_key' => 'order_custom_field_id')
    );
}