<?php

namespace Msst\MACompany;

class OrderCustomFieldOption extends \Msst\BaseModel {

    static $table_name = 'ma_system_setup_order_custom_field_options';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('macompany', 'class' => 'Msst\MACompany', 'foreign_key' => 'ma_company_id'),
        array('custom_field', 'class' => 'Msst\MACompany\OrderCustomField', 'foreign_key' => 'order_custom_field_id')
    );
}