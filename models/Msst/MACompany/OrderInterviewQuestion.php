<?php

namespace Msst\MACompany;

class OrderInterviewQuestion extends \Msst\BaseModel {

    static $table_name = 'ma_system_setup_order_interview_custom_questions';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('macompany', 'class' => 'Msst\MACompany', 'foreign_key' => 'ma_company_id')
    );

    static $has_many = array(
        array('options', 'class' => 'Msst\MACompany\OrderInterviewOption', 'foreign_key' => 'custom_question_id')
    );

}