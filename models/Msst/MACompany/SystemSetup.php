<?php

namespace Msst\MACompany;

class SystemSetup extends \Msst\BaseModel {

    static $table_name = 'ma_system_setup';

    static $primary_key = 'ma_company_id';

    static $belongs_to = array(
        array('macompany', 'class' => 'Msst\MACompany', 'foreign_key' => 'ma_company_id')
    );

    /**
     * Return esign setup object.
     * if not present, create a new one.
     * It can be accessed by $this->esign, or $object->esign
     *
     * @return \Msst\MACompany\EsignSetup | NULL, return null when eSign feature is not enabled
     *         on the ma company level
     */
    public function get_esign() {
        if ($this->isFeatureEnabled(\Msst\MACompany::FEATURE_DOCUSIGN)) {
            $esign_setup = \Msst\MACompany\EsignSetup::first(array(
                'conditions' => array('ma_company_id = ?', $this->ma_company_id))
            );
            
            if (!$esign_setup) {
                $esign_setup = \Msst\MACompany\EsignSetup::create(
                    array('ma_company_id' => $this->ma_company_id)
                );
            }

            return $esign_setup;
        } else {
            return null;
        }
    }

    /**
     * Check if a feature or feature group is set on this company
     * Multiple features can be grouped together, eg:
     * $this->disable(self::FEATURE_A | self::FEATURE_B);
     * Note, use integers other than class constants defined above would unpredictable result.
     *
     * @param int $feature
     * @return boolean
     */
    public function isFeatureEnabled($feature) {
        return (bool) ($feature & $this->featureset);
    }

    /**
     * Set the bit flag represeting a feature.
     * Multiple features can be grouped together, eg:
     * $this->enableFeature(self::FEATURE_A | self::FEATURE_B);
     * Note, use integers other than class constants defined above would have unpredictable result.
     *
     * @param int $feature
     */
    public function enableFeature($feature) {
        $this->featureset |= $feature;
        return $this->save();
    }

    /**
     * Clear the bit flag represeting a feature.
     * Multiple features can be grouped together, eg:
     * $this->disableFeature(self::FEATURE_A | self::FEATURE_B);
     * Note, use integers other than class constants defined above would have unpredictable result.
     *
     * @param int $feature
     */
    public function disableFeature($feature) {
        $this->featureset &= ~$feature;
        return $this->save();
    }

}