<?php

namespace Msst\MACompany;

class EsignCredential extends \Msst\BaseModel {

    static $table_name = 'ma_system_setup_esign_credential';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('macompany', 'class' => 'Msst\MACompany', 'foreign_key' => 'ma_company_id')
    );

}