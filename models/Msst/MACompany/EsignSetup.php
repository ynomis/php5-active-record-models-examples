<?php

namespace Msst\MACompany;

class EsignSetup extends \Msst\BaseModel {

    static $table_name = 'ma_system_setup_esign';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('macompany', 'class' => 'Msst\MACompany', 'foreign_key' => 'ma_company_id'),
        array('system_setup', 'class' => 'Msst\MACompany\SystemSetup', 'foreign_key' => 'ma_company_id')
    );

}