<?php

namespace Msst;

/**
 * Client Company.
 * Table name: client_companies
 *
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class ClientCompany extends \Msst\BaseModel {

    static $table_name = 'client_companies';

    static $primary_key = 'id';

    static $validates_presence_of = array(
        array('ma_company_id', 'message' => 'How come you get to this point without a valid MA company?'),
        array('agent_id', 'message' => 'The client company must belong to an agent.'), array('company_name')
    );

    const REGEX_USPHONE = '/^[2-9]\d{2}-\d{3}-\d{4}$/';

    static $validates_format_of = array(
		// array('general_phone',	'with' => self::REGEX_USPHONE, 'allow_null' => true ),
		// array('general_fax',	'with' => self::REGEX_USPHONE, 'allow_null' => true ),
    );

    static $before_save = array(
        'check_duplicate_company_name',
        'set_update_date'
    );

    static $before_create = array(
        'check_agent_is_valid',
        'set_default_values',
        'set_create_date'
    );

    static $before_destroy = array(
        'cleanup_references'
    );

    /**
     *
     * Make sure the given agentid belongs to the company when create a new
     * client company
     *
     * @return boolean
     */
    public function check_agent_is_valid() {
        try {
            $agent = \Msst\Agent::find($this->agent_id);
            
            if ($agent->master_agent->ma_company_id != $this->ma_company_id) {
                $this->errors->add('agent_id', 'This agent id \'' . $this->agent_id . '\' doesn\'t belong to the company.');
                return false;
            }
        } catch (\ActiveRecord\RecordNotFound $e) {
            $this->errors->add('agent_id', 'I can not find enough info for agent \'' . $this->agent_id . '\'.');
            return false;
        }
        $this->assign_attribute('promo_code', $agent->promo_code);
    }

    /**
     *
     *
     * Make sure there are not 2 copmanies can have the same name
     *
     * @return boolean
     */
    public function check_duplicate_company_name() {
        if ($this->is_dirty() && array_key_exists('company_name', $this->dirty_attributes())) {
            if (static::exists(array(
                'conditions' => array('agent_id=? AND LOWER(company_name)=?', $this->agent_id, strtolower($this->company_name))
            ))) {
                $this->errors->add('company_name', 'Duplicate, \'' . $this->company_name . '\' is not allowed.');
                return false;
            }
        }
    }

    /**
     * Set create date
     */
    public function set_create_date() {
        $this->assign_attribute('signup_date', date('Y-m-d'));
    }

    /**
     * Set update date
     */
    public function set_update_date() {
        $this->assign_attribute('last_updated', date('Y-m-d'));
    }

    /**
     * Set default values
     */
    public function set_default_values() {
        $this->assign_attribute('employee_id', 1);
        $this->set_promocode_from_agent();
    }

    /**
     * Set promocode when creating a new clinet company
     *
     * @return boolean
     */
    protected function set_promocode_from_agent() {
        try {
            $agent = \Msst\Agent::find($this->agent_id);
        } catch (\ActiveRecord\RecordNotFound $e) {
            $this->errors->add('agent_id', 'I can not find enough info for agent \'' . $this->agent_id . '\'.');
            return false;
        }
        $this->assign_attribute('promo_code', $agent->promo_code);
    }

    public function delete() {
        if ($this->is_deleteable()) {
            try {
                $this->connection()->transaction();
                if (parent::delete()) {
                    $this->connection()->commit();
                    return true;
                } else {
                    $this->connection()->rollback();
                    return false;
                }
            } catch ( \Exception $e ) {
                $this->connection()->rollback();
                error_log($e->getMessage());
                return false;
            }
        } else {
            error_log('This client company [' . $this->id . '] was not deleted, because it still has active orders, rfqs or opportunities.');
            return false;
        }
    }

    /**
     * Upon deleting this record, all associated reference in db and file system needs
     * to be cleaned up.
     * This is wrapped into a transaction, see $this->delete() for
     * detail. However the file deletion may be out of sync, because they can not be
     * part of the rollback.
     */
    public function cleanup_references() {
        // cleanup related table records
        \Msst\Client::delete_all(array(
            'conditions' => array('client_company_id = ?', $this->id))
        );
        \Msst\ClientCompany\Account::delete_all(array(
            'conditions' => array('client_company_id = ?', $this->id))
        );
        \Msst\ClientCompany\Location::delete_all(array(
            'conditions' => array('client_company_id = ?', $this->id))
        );
        \Msst\ClientCompany\Note::delete_all(array(
            'conditions' => array('client_company_id = ?', $this->id))
        );
        \Msst\ClientCompany\Requirement::delete_all(array(
            'conditions' => array('client_company_id = ?', $this->id))
        );

        // cleanup file references
        $uploadfiles_options = array(
            'conditions' => array('key_field = ? AND key_value = ? AND ma_company_id = ?', 'client_company_id', $this->id, $this->ma_company_id)
        );
        $files = \Msst\UploadFile::all($uploadfiles_options);
        
        $dbname = static::connection()->dbname;
        $rootUploadFolder = "/usr/local/uploadfiles/{$dbname}/{$this->ma_company_id}";
        $finalUploadFolder = $rootUploadFolder . "/client_companies";
        
        foreach ($files as $_file) {
            $filename = $_file->filename;
            $file_id = $_file->id;
            
            $unwantedFileExt = strtolower(substr(strrchr($filename, "."), 1));
            $unwantedFile = $finalUploadFolder . "/" . str_replace("{$unwantedFileExt}", "", $filename) . "#{$file_id}{$unwantedFileExt}";
            
            if (is_file($unwantedFile))
                @unlink($unwantedFile);
        }
        
        \Msst\UploadFile::delete_all($uploadfiles_options);
    }

    /**
     * Check if this client company has the location by given id
     *
     * @param int $location_id
     * @return boolean, true|false
     */
    public function has_location($location_id) {
        return \Msst\ClientCompany\Location::exists(array(
            'conditions' => array('id = ? AND client_company_id = ?', $location_id, $this->id))
        );
    }

    /**
     * Used mainly by validation on delete.
     *
     * @return boolean, true - not having orders, ok to delete;
     *         false - having orders, not ok to delete
     */
    public function check_having_orders() {
        if (!$this->id)
            return false;
        
        $return = !(bool) Order::count(array(
            'conditions' => array('client_company_id = ?', $this->id))
        );
        
        if (!$return) {
            if (!$this->errors) {
                $this->errors = new \ActiveRecord\Errors($this);
            }
            $this->errors->add('id', $this->id . " You may not delete client company [{$this->id}] when it has orders.");
        }
        
        return $return;
    }

    /**
     * Used mainly by validation on delete
     *
     * @return boolean, true - not having any related opportunities, ok to delete
     *         false - having related opportunities, not ok to delete
     */
    public function check_having_opportunities() {
        if (!$this->id)
            return false;
        
        $return = !(bool) Opportunity::count(array(
            'conditions' => array('client_company_id = ?', $this->id))
        );
        
        if (!$return) {
            if (!$this->errors) {
                $this->errors = new \ActiveRecord\Errors($this);
            }
            $this->errors->add('id', $this->id . " You may not delete client [{$this->id}] when it has opportunities.");
        }
        
        return $return;
    }

    /**
     * Used manily by validation on delete
     *
     * @return boolean, true - not having RFQs for this client company, ok to delete
     *         false - having RFQs, not ok to delete
     */
    public function check_having_rfqs() {
        if (!$this->id)
            return false;
        
        $return = !(bool) RFQ::count(array(
            'joins'         => 'INNER JOIN clients c ON(rfq.client_id = c.id)',
            'conditions'    => array('c.client_company_id = ?', $this->id)
        ));
        
        if (!$return) {
            if (!$this->errors) {
                $this->errors = new \ActiveRecord\Errors($this);
            }
            $this->errors->add('id', $this->id . " You may not delete client [{$this->id}] when there are rfqs bound to it.");
        }
        
        return $return;
    }

    /**
     * Convenient function to aggregate all check functions
     *
     * @return boolean, true - ok to delete; false not ok.
     */
    public function is_deleteable() {
        return $this->check_having_rfqs() && $this->check_having_orders() && $this->check_having_opportunities();
    }

    /**
     * Helper to locate client companies by ma_company_id and agent_id
     *
     * @param int $maCompanyId
     * @param int $agentId
     * @param int $limit
     * @param int $offset
     * @return Array<Msst\ClientCompany>
     */
    public static function get_client_companies($maCompanyId, $agentId, $limit = null, $offset = null) {
        $options = array(
            'conditions' => array('ma_company_id = ? AND agent_id = ?', $maCompanyId, $agentId), 'order' => 'company_name');
        $options['limit']   = $limit;
        $options['offset']  = $offset;

        return static::all($options);
    }
}