<?php

namespace Msst\Order;

class Payment extends \Msst\BaseModel {

    static $table_name = 'payments';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('order', 'class' => 'Msst\Order', 'foreign_key' => 'order_id')
    );

    const METHOD_CREDITCARD = 'Credit Card';
    const METHOD_OTHERS     = 'Others';
    const STATUS_COMPLETE   = 'Complete';
    const STATUS_PENDING    = 'Pending';
    const STATUS_DELIVERED  = 'Delivered';

}