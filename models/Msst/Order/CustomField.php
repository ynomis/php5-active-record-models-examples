<?php

namespace Msst\Order;

class CustomField extends \Msst\BaseModel {

    static $table_name = 'order_custom_fields';

    static $primary_key = array('ma_company_id', 'order_id', 'order_custom_field_id');

    static $belongs_to = array(
        array('order', 'class' => 'Msst\Order', 'foreign_key' => 'order_id')
    );

}