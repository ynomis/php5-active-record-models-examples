<?php

namespace Msst\Order;

class Note extends \Msst\BaseModel {

    static $table_name = 'order_notes';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('order', 'class' => 'Msst\Order', 'foreign_key' => 'order_id')
    );

}