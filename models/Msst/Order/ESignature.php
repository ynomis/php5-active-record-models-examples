<?php

namespace Msst\Order;

/* Some of actions depend on DocuSign models and API */
require_once __DIR__ . '/../../../connector/Docusign/autoload.php';

use ActiveRecord\DateTime;

class ESignature extends \Msst\BaseModel {

    static $table_name = 'order_extension_esign';

    static $primary_key = 'id';

    /**
     * This may not work, 'cause some quote doesnt have order id! In that case
     * use quote_sq_id to look it up.
     */
    static $belongs_to = array(
        array('order', 'class' => 'Msst\Order', 'foreign_key' => 'order_id'),
        array('quote', 'class' => 'Msst\Quote', 'foreign_key' => 'quote_sq_id')
    );

    /**
     * This is a convenience interface to query e-Signature (DocuSign) envelope status.
     * Based on DocuSign's TOC, API call would be only allowed for 15 minutes in between.
     * Note, since it involves polling external service, it may take longer time to
     * response. Do allow enough execution time for it, or gracefully exit with exception.
     */
    public function getLatestStatus() {
        $chktime = new \ActiveRecord\DateTime();
        $chktime->sub(new \DateInterval('PT15M'));
        
        if ($this->last_status_check < $chktime) {
            $rfq = $this->quote->get_rfq(); // just want to grad the ma_company_id
            try {
                $api = \Docusign\API::getInstanceFromCompany($rfq->ma_company_id, $this->agent_id);
                
                /* @var $envlp \Docusign\Model\Response\Envelope */
                $envlp = $api->getResponseEnvelope($this->envelope_id);
                /* @var $rcps \Docusign\Model\Response\Recipients */
                $rcps = $api->getResponseRecipients($this->envelope_id);
            } catch (Docusign\ApiException $e) {
                error_log($e->getMessage());
                return false;
            }
            
            try {
                $this->status = $envlp->status;
                $this->last_mod = $envlp->statusChangedDateTime;
                $this->last_status_check = new \ActiveRecord\DateTime();
                $this->last_status_cache = json_encode(array(serialize($envlp), serialize($rcps)));
                
                if (!$this->save()) {
                    error_log($this->errors->full_messages());
                    return false;
                }
            } catch (\PDOException $e) {
                error_log($e->getMessage());
                return false;
            }
        } elseif (!empty($this->last_status_cache)) {
            list($envlp, $rcps) = json_decode($this->last_status_cache);
            $envlp = unserialize($envlp);
            $rcps = unserialize($rcps);
        } else {
            error_log('What the hell, no cache from last polling?');
            return false;
        }
        
        return array($envlp, $rcps);
    }
}