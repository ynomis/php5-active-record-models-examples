<?php

namespace Msst\Order;

class Location extends \Msst\BaseModel {

    static $table_name = 'order_locations';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('order', 'class' => 'Msst\Order', 'foreign_key' => 'order_id')
    );

}