<?php

namespace Msst\Quote;

class LocationQuote extends \Msst\BaseModel {

    static $table_name = 'quote_sq_per_loc';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('quote', 'class' => 'Msst\Quote', 'foreign_key' => 'quote_sq_id'),
        array('loc_requirement', 'class' => '\Msst\Requirement\LocationRequirement', 'foreign_key' => 'req_sq_idx_id'));

    static $has_many = array(
        array('skus', 'class' => 'Msst\Quote\SKU', 'foreign_key' => 'quote_sq_per_loc_id')
    );

    /**
     * A reference to its rfq object.
     * May need this to check ma_company_id.
     * So user can use:
     * $loc_quote = \Msst\Quote\LocationQuote::find(1);
     * $rfq = $loc_quote->rfq; /* var \Msst\RFQ $rfq *\/
     *
     * @return \Msst\RFQ | null, if not found
     */
    public function get_rfq() {
        return \Msst\RFQ::first(array(
            'joins' => 'INNER JOIN rfq_idx ON rfq_idx.rfq_id = rfq.id
						INNER JOIN quote_sq ON quote_sq.rfq_idx_id = rfq_idx.id
						INNER JOIN quote_sq_per_loc ON quote_sq_per_loc.quote_sq_id = quote_sq.id',
            'conditions' => array('quote_sq_per_loc.id = ?', $this->id)
        ));
    }

}