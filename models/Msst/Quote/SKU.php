<?php

namespace Msst\Quote;

class SKU extends \Msst\BaseModel {

    static $table_name = 'quote_sq_skus';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('quote', 'class' => 'Msst\Quote', 'foreign_key' => 'quote_sq_id'),
        array('loc_quote', 'class' => '\Msst\Quote\LocationQuote', 'foreign_key' => 'quote_sq_per_loc_id')
    );

}