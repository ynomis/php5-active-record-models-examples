<?php

namespace Msst\RFQ;

class RFQIdx extends \Msst\BaseModel {

    static $table_name = 'rfq_idx';

    static $primary_key = 'id';

    static $belongs_to = array(
        array('rfq', 'class' => 'Msst\RFQ', 'foreign_key' => 'rfq_id')
    );

}