<?php
defined('DOC_ROOT') or define('DOC_ROOT', realpath(__DIR__ . "/../"));

require_once DOC_ROOT . "/inc/Config.php";

$conf = new Config($_SERVER['SERVER_NAME']);

/**
 * This is to load all models under this directory. Should this folder move
 * to other place, or php-activerecord packge is moved. following line needs
 * to be adjusted accordingly.
 *
 * require this file to autoload all model classes in Msst namespace, for now.
 * Ideally, it will be wrapped up into bootstrap script on application level.
 */
require_once DOC_ROOT . '/inc/php-activerecord/ActiveRecord.php';

\ActiveRecord\Config::initialize(function ($cfg) use($conf) {
    $cfg->set_model_directory(__DIR__);
    $cfg->set_connections(array(
        'devvm'         => "pgsql://user:password@{$conf->getDevDbHost()}:8100/{$conf->getCurrentDb()}",
        'stage'         => '',
        'production'    => "pgsql://user:password@{$conf->getLiveDbHost()}:8100/MAM",
        'unittest'      => 'mockpgsql://user:password@localhost:8100/DEV_MAM'));
    
    if ('Live' == $conf->getCodeType()) {
        $cfg->set_default_connection('production');
    } else {
        $cfg->set_default_connection('devvm');
    }
});
